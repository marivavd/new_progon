import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class ChangePassUseCase{
  Future<void> pressButtonChangePass(
      String password,
      String confirmpassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmpassword){
      onError('Passwords dont match');
    }
    else{
      requestChangePass() async{
        await changePass(password);
      }
      await requests(requestChangePass, onResponse, onError);
    }



  }
}
