import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class LogOutUseCase{
  Future<void> pressButtonLogOut(
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestLogOut() async{
      await log_out();
    }
    await requests(requestLogOut, onResponse, onError);



  }
}