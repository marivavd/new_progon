

import 'package:new_progon/data/models/model_profile.dart';
import 'package:new_progon/data/repository/requests.dart';

Future<ModelProfile> makeProfile()async{
  final data = await get_user();
  return ModelProfile(fullname: data['fullname'], balance: data['balance'].toString(), avatar: await getMyAvatar());
}