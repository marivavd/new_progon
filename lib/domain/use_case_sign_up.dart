import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButtonSignUp(
      String fullname,
      String phone,
      String email,
      String password,
      String confirmpassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmpassword){
      onError('Passwords dont match');
    }
    else{
      requestSignUp() async{
        await signUp(email: email, password: password, fullname: fullname, phone: phone);
      }
      await requests(requestSignUp, onResponse, onError);
    }



  }
}
