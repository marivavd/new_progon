

import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class UseCaseFeedbackRates{
  Future<void> pressButtonFeedbackRates(
      String feedback,
      int rates,
      String id,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestFeedbackRates() async{
      await feedback_and_rate(rates, feedback, id);
    }
    await requests(requestFeedbackRates, onResponse, onError);



  }

}