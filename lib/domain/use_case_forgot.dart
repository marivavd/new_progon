import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class ForgotUseCase{
  Future<void> pressButtonForgot(
      String email,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestForgot() async{
      await sendEmail(email);
    }
    await requests(requestForgot, onResponse, onError);



  }
}
