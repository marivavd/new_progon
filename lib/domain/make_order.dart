import 'package:flutter/cupertino.dart';
import 'package:new_progon/data/models/model_order.dart';
import 'package:new_progon/data/repository/requests.dart';

Future<void> make_order(
    String address,
    String country,
    String phone,
    String others,
    String items,
    String weight,
    String worth,
    List<TextEditingController> adresses,
    List<TextEditingController> countries,
    List<TextEditingController> phones,
    List<TextEditingController> otherses,
    Function(ModelOrder) onResponse,
    Function(String) onError
)async{
  List<Map<String, String>> destinations = [];
  for (int i = 0; i < adresses.length; i++){
    destinations.add(
        {
          "address": adresses[i].text,
          "country": countries[i].text,
          "phone": phones[i].text,
          "others": otherses[i].text,
          "id_order": ''
        }
    );
  }
  try{
    double delivery_charges = adresses.length * 2500;
    double instantDelivery = 300.00;
    double tax = (delivery_charges + instantDelivery) * 0.05;
    double sum_price = tax + instantDelivery + delivery_charges;

    String idOrder = await insertNewOrder(
        address,
        country,
        phone,
        others,
        items,
        int.parse(weight),
        int.parse(worth),
        delivery_charges,
        instantDelivery,
        tax,
        sum_price,
        destinations);
    onResponse(ModelOrder(id: idOrder, phone: phone, sum_price: sum_price, tax: tax, delivery_charges: delivery_charges, worth: int.parse(worth), weight: int.parse(weight), items: items, country: country, address: address, instantDelivery: instantDelivery, others: others));

  }
  catch (e){
    onError(e.toString());
  }


}