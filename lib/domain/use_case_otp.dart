import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class OTPUseCase{
  Future<void> pressButtonOTP(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestOTP() async{
      await verifyOTP(email, code);
    }
    await requests(requestOTP, onResponse, onError);



  }
}
