import 'package:new_progon/data/models/map_point.dart';
import 'package:new_progon/data/repository/requests.dart';

Future<List<MapPoint>> makeMapPoints()async {
  final data = await getMapPoints();
  List<MapPoint> result = [];
  for (int i = 0; i < data.length; i ++){
    result.add(MapPoint(name: '${data[i]['id']}-start', latitude: double.tryParse(data[i]['lat_start'].toString())??0.0, longitude: double.tryParse(data[i]['lon_start'].toString())??0.0));
    result.add(MapPoint(name: '${data[i]['id']}-end', latitude: double.tryParse(data[i]['lat_end'].toString())??0.0, longitude: double.tryParse(data[i]['lon_end'].toString())??0.0));
  }
  return result;
}