import 'package:new_progon/data/repository/requests.dart';
import 'package:new_progon/domain/utils.dart';

class SignInUseCase{
  Future<void> pressButtonSignIn(
      String email,
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestSignIn() async{
      await signIn(email: email, password: password);
    }
    await requests(requestSignIn, onResponse, onError);



  }
}
