import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/presentation/pages/map.dart';
import 'package:new_progon/presentation/pages/sign_up.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/theme/colors.dart';
import 'package:new_progon/presentation/theme/theme.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://uboklrrvwysdoztwfvuj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVib2tscnJ2d3lzZG96dHdmdnVqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcyOTI1MjEsImV4cCI6MjAyMjg2ODUyMX0.snO7gjARZRw677ToKsGYc49lgiHIU7HCaCw6M7rIXwY',
  );

  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;





class MyApp extends StatefulWidget {
  MyApp({super.key});
  bool isLight = true;

  void changeDiffTheme(BuildContext context) {
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()?.onChangeTheme();
  }

  ColorsApp getColors(BuildContext context) {
    return (isLight) ? colorsLights : colorsDark;
  }

  ThemeData getCurrentTheme() {
    return (isLight) ? light : dark;
  }

  static MyApp of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }



  @override
  State<MyApp> createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> connectivitySubscription;

  void onChangeTheme() {
    setState(() {});
  }



  @override
  void initState() {
    super.initState();
    connectivitySubscription = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){
        showError(context, 'Internet failed');
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    connectivitySubscription.cancel();
  }




  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: SignUp(),
    );
  }

}





