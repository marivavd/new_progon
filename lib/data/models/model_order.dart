class ModelOrder {
  String id;
  int status;
  String address;
  String country;
  String phone;
  String others;
  String items;
  int weight;
  int worth;
  double delivery_charges;
  double instantDelivery;
  double tax;
  double sum_price;


  ModelOrder(
      {required this.id,
        this.status = 0,
        required this.phone,
        required this.sum_price,
        required this.tax,
        required this.delivery_charges,
        required this.worth,
        required this.weight,
        required this.items,
        required this.country,
        required this.address,
        required this.instantDelivery,
        required this.others,
      });
}
