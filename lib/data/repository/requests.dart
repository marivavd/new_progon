import 'package:new_progon/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<List<Map<String, dynamic>>> getMapPoints()async{
  return await supabase.from('points').select();
}

Future<void> signIn(
    {required String email,
      required String password,
    })async{
    final AuthResponse res = await supabase.auth.signInWithPassword(
      email: email,
      password: password,
    );
}

Future<void> signUp(
    {required String email,
      required String password,
      required String fullname,
      required String phone,
    })async{
  final AuthResponse res = await supabase.auth.signUp(
          email: email,
          password: password,
        );
        await supabase
            .from('profiles')
            .insert({'fullname': fullname, 'phone': phone, "id_user": supabase.auth.currentUser!.id});
}





Future<void> sendEmail(String email,)async{
  return await supabase.auth.resetPasswordForEmail(email);
}



Future<AuthResponse> verifyOTP(
    String email,
    String code
    )async{

  return await supabase.auth.verifyOTP(
      type: OtpType.email,
      token: code,
      email: email);
}


Future<UserResponse> changePass(String pass)async {
  return await supabase.auth.updateUser(
    UserAttributes(
        password: pass
    ),
  );
}

Future<Map<String, dynamic>> get_user()async{
  return await supabase.from('profiles').select().eq('id_user', supabase.auth.currentUser!.id).single();
}


Future<dynamic> getMyAvatar()async{
  final profile = await supabase.from('profiles').select().eq('id_user', supabase.auth.currentUser!.id).single();
  if (profile['avatar'] == ''){
    return null;
  }
  var file = await supabase.storage.from('avatars').download(profile['avatar']);
  return file;
}

Future<void> log_out()async{
  return await supabase.auth.signOut();
}

Future<void> feedback_and_rate(stars, feedback, id) async{
  await supabase
      .from('orders')
      .update({'feedback': feedback, 'rate': stars}).match({"id": id});}


Future<String> insertNewOrder(
    String address,
    String country,
    String phone,
    String others,
    String items,
    int weight,
    int worth,
    double delivery_charges,
    double instantDelivery,
    double tax,
    double sum_price,
    List<Map<String, String>> destinations) async {
  String idOrder = await supabase
      .from("orders")
      .insert({
    "id_user": supabase.auth.currentUser!.id,
    "address": address,
    "country": country,
    "phone": phone,
    "others": others,
    "package_items": items,
    "weight_items": weight,
    "worth_items": worth,
    "delivery_charges": delivery_charges,
    "instant_delivery": instantDelivery,
    "tax_and_service_charges": tax,
    "sum_price": sum_price
  })
      .select()
      .single()
      .then((value) => value["id"]);

  for (var d in destinations) {
    d["id_order"] = idOrder;
    await supabase.from("destinations_details").insert(d).select().single();
  }
  return idOrder;
}
Future<void> subscribeOrder(String orderId, callback) async {
  supabase
      .channel("orders-status-changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.update,
      schema: "public",
      table: "orders",
      filter: PostgresChangeFilter(
          type: PostgresChangeFilterType.eq,
          column: "id",
          value: orderId
      ),
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}
Future<List<Map<String, dynamic>>> getTransactions() async{
  return await supabase.from("transactions").select().eq("id_user", supabase.auth.currentUser!.id);
}
Future<Map<String, dynamic>> getOrder() async{
  return await supabase.from("orders").select().eq("id_user", supabase.auth.currentUser!.id).single();
}
Future<List<Map<String, dynamic>>> get_orders() async{
  return await supabase.from("orders").select().eq("id_user", supabase.auth.currentUser!.id);
}
Future<List<Map<String, dynamic>>> getDestinationDetailsOrder(String id_order) async{
  return await supabase.from("destinations_details").select().eq("id_order", id_order);
}
