import 'package:new_progon/domain/make_map_points.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
Future<ClusterizedPlacemarkCollection> createCluster()async{
  final data = await makeMapPoints();
  List<PlacemarkMapObject> placemarkObjects = [];
  for (int i = 0; i < data.length; i ++){
    placemarkObjects.add(
        PlacemarkMapObject(
            mapId: MapObjectId(data[i].name),
            point: Point(latitude: data[i].latitude, longitude: data[i].longitude),
          opacity: 1,
          icon: PlacemarkIcon.single(
            PlacemarkIconStyle(
              image: BitmapDescriptor.fromAssetImage('assets/orange.png'),
              scale: 5
            ),
          ),
        ));
  }

  final cluster = ClusterizedPlacemarkCollection(
      mapId: MapObjectId('cluster'),
      placemarks: placemarkObjects,
      radius: 30,
      minZoom: 15);
  return cluster;
}