import 'dart:async';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/domain/controllers/password_controller.dart';
import 'package:new_progon/domain/use_case_sign_in.dart';
import 'package:new_progon/domain/use_case_sign_up.dart';
import 'package:new_progon/domain/utils.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:new_progon/presentation/pages/forgot.dart';
import 'package:new_progon/presentation/pages/home_page.dart';
import 'package:new_progon/presentation/pages/sign_up.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/custom_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});


  @override
  State<SignIn> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignIn> {
  var email = TextEditingController(text: 'mar@gmail.com');
  var password = PasswordController();
  bool check = false;
  SignInUseCase useCase = SignInUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                'Welcome Back',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Fill in your email and password to continue',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 20,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 24,),
              CustomField(label: 'Password', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),

              SizedBox(height: 17,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      child: Row(
                        children: [
                          SizedBox(width: 1,),
                          SizedBox(
                            height: 14,
                            width: 14,
                            child: Checkbox(
                              value: check,
                              side: BorderSide(
                                  color: Color(0xFFA7A7A7),
                                  width: 1
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2)
                              ),
                              activeColor: Color(0xFF006CEC),
                              onChanged: (val){
                                setState(() {
                                  check = val!;
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 4,),
                          Text(
                              "Remember password",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12)
                          )
                        ],
                      )),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Forgot_pass())).then((value) => setState(() {

                      }));
                    },
                    child: Text(
                        "Forgot Password?",
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12, color: Color(0xFF0560FA))
                    ),
                  )
                ],
              ),
              SizedBox(height: 187,),

              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressButtonSignIn(
                          email.text,
                          password.hashPassword(),
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(current_index: 0,))).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          });
                    }: null,
                    child: Text(
                      "Log in",
                      style: Theme.of(context).textTheme.labelLarge
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Already have an account?",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
                    },
                    child: Text(
                      "Sign Up",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Column(
                  children: [
                    Text(
                      "or log in using",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    SizedBox(height: 8,),
                    Image.asset("assets/Facebook google, apple.png")
                  ],
                )],
              ),
              SizedBox(height: 28,)


            ],

          ),
        ),
      ),
    );
  }
}
