import 'dart:async';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/domain/controllers/password_controller.dart';
import 'package:new_progon/domain/use_case_forgot.dart';
import 'package:new_progon/domain/use_case_otp.dart';
import 'package:new_progon/domain/use_case_sign_in.dart';
import 'package:new_progon/domain/use_case_sign_up.dart';
import 'package:new_progon/domain/utils.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:new_progon/presentation/pages/change.dart';
import 'package:new_progon/presentation/pages/home_page.dart';
import 'package:new_progon/presentation/pages/sign_in.dart';
import 'package:new_progon/presentation/pages/sign_up.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/custom_field.dart';
import 'package:pinput/pinput.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class OTP extends StatefulWidget {
  OTP({super.key, required this.email});
  String email;


  @override
  State<OTP> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OTP> {
  OTPUseCase useCase = OTPUseCase();
  bool isValid = false;
  var controller = TextEditingController();
  bool is_Error = false;
  ForgotUseCase useCaseForgot = ForgotUseCase();







  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var separator = width / 6 - 32;
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                'OTP Verification',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Enter the 6 digit numbers sent to your email',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 55,),
              Pinput(
                length: 6,
                controller: controller,
                separatorBuilder: (context) => SizedBox(width: separator,),
                defaultPinTheme: PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.zero,
                        border: Border.all(
                            width: 1,
                            color: Color(0xFFA7A7A7)
                        )
                    )
                ),
                focusedPinTheme: PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.zero,
                        border: Border.all(
                            width: 1,
                            color: Color(0xFF0560FA)
                        )
                    )
                ),
                submittedPinTheme: (!is_Error)?PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.zero,
                        border: Border.all(
                            width: 1,
                            color: Color(0xFF0560FA)
                        )
                    )
                ): PinTheme(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.zero,
                        border: Border.all(
                            width: 1,
                            color: Color(0xFFED3A3A)
                        )
                    )
                ),
                onChanged: (text){
                  setState(() {
                    is_Error = false;
                    isValid = text.length == 6;
                  });
                },
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "If you didn’t receive code, ",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    child: Text(
                        "resend",
                        style: TextStyle(
                            color: Color(0xFF0560FA),
                            fontWeight: FontWeight.w400,
                            fontSize: 14
                        )
                    ),
                    onTap: ()async{
                      showLoading(context);
                      useCaseForgot.pressButtonForgot(
                          widget.email,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: widget.email))).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          });
                    },
                  )
                ],
              ),
              SizedBox(height: 84,),

              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressButtonOTP(
                          widget.email,
                              controller.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePass())).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          });
                    }: null,
                    child: Text(
                        "Set New Password",
                        style: Theme.of(context).textTheme.labelLarge
                    ),
                  ),
                ),
              ),

            ],

          ),
        ),
      ),
    );
  }
}
