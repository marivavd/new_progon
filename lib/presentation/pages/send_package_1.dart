import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:new_progon/data/models/model_order.dart';
import 'package:new_progon/domain/make_order.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/pages/send_package_2.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import '../widgets/text_field.dart';

class SendPackage1 extends StatefulWidget {
  const SendPackage1({super.key});

  @override
  State<SendPackage1> createState() => _SendPackage1State();
}

class _SendPackage1State extends State<SendPackage1> {
  var address_controller = TextEditingController();
  var country_controller = TextEditingController();
  var phone_controller = TextEditingController();
  var others_controller = TextEditingController();
  var items_controller = TextEditingController();
  var weight_controller = TextEditingController();
  var worth_controller = TextEditingController();
  int count = 1;
  List<TextEditingController> adresses = [TextEditingController()];
  List<TextEditingController> countries = [TextEditingController()];
  List<TextEditingController> phones = [TextEditingController()];
  List<TextEditingController> otherses = [TextEditingController()];



  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(color: colors.fFFFFF, boxShadow: [
                BoxShadow(
                    color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
              ]),
              child: Stack(children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text("Send a package",
                    style: TextStyle(
                        color: colors.a7A7A7,
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                    ),),
                ),
                Align(alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset('assets/arrow-square-right.svg', color: colors.o560FA),
                    ))
              ]),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: 32)),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    children: [
                      SvgPicture.asset("assets/origin.svg", color: colors.o560FA,),
                      SizedBox(width: 8,),
                      Text("Origin Details",
                        style: TextStyle(
                          fontSize: 14,
                          color: colors.x3A3A3A,
                          fontWeight: FontWeight.w500
                        ))
                    ],
                  ),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "Address", controller: address_controller),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "State,Country", controller: country_controller),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "Phone number", controller: phone_controller),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "Others", controller: others_controller),
                ],
              ),
            ),
          ),
          SliverList.builder(
            itemCount: count,
            itemBuilder: (_, index){
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Container(
                  child: Column(
                    children: [
                      SizedBox(height: 39,),
                      Row(
                        children: [
                          SvgPicture.asset("assets/destination.svg", color: colors.o560FA,),
                          SizedBox(width: 8,),
                          Text("Destination Details",
                            style: TextStyle(
                              fontSize: 14,
                              color: colors.x3A3A3A,
                              fontWeight: FontWeight.w500
                          ))
                        ],
                      ),
                      SizedBox(height: 5,),
                      Custom_Field(hint: "Address", controller: adresses[index]),
                      SizedBox(height: 5,),
                      Custom_Field(hint: "State,Country", controller: countries[index]),
                      SizedBox(height: 5,),
                      Custom_Field(hint: "Phone number", controller: phones[index]),
                      SizedBox(height: 5,),
                      Custom_Field(hint: "Others", controller: otherses[index]),
                      SizedBox(height: 10,),
                    ],
                  ),
                ),
              );
            },
          ),
          SliverToBoxAdapter(
              child:  Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          count ++;
                          adresses.add(TextEditingController());
                          phones.add(TextEditingController());
                          countries.add(TextEditingController());
                          otherses.add(TextEditingController());
                        });
                      },
                      child: SvgPicture.asset("assets/add-square.svg", color: colors.o560FA,),
                    ),

                    SizedBox(width: 2,),
                    Text("Add destination",
                      style: TextStyle(
                          fontSize: 12,
                          color: colors.a7A7A7,
                          fontWeight: FontWeight.w400
                      ),)
                  ],
                ),
              )
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  SizedBox(height: 13,),
                  Row(children: [Text("Package Details", style: TextStyle(
                      fontSize: 14,
                      color: colors.x3A3A3A,
                      fontWeight: FontWeight.w500
                  ),),],),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "package items", controller: items_controller),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "Weight of item(kg)", controller: weight_controller),
                  SizedBox(height: 5,),
                  Custom_Field(hint: "Worth of Items", controller: worth_controller),
                  SizedBox(height: 39,),
                  Row(children: [Text("Select delivery type", style: TextStyle(
                      fontSize: 14,
                      color: colors.x3A3A3A,
                      fontWeight: FontWeight.w500
                  )),],),
                  SizedBox(height: 16,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                              height: 75,
                              width: 159,
                              child: FilledButton(
                                style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll<Color>(colors.o560FA,),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        side: BorderSide(width: 0, color: colors.o560FA),
                                        borderRadius: BorderRadius.circular(8),

                                      )
                                  ),
                                ),
                                onPressed: ()async{
                                  showLoading(context);
                                  await make_order(address_controller.text,
                                      country_controller.text,
                                      phone_controller.text,
                                      others_controller.text,
                                      items_controller.text,
                                      weight_controller.text,
                                      worth_controller.text,
                                      adresses, countries,
                                      phones,
                                      otherses,
                                      (order){
                                    hideLoading(context);
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => SendPackage2(order: order)));},
                                      (e){
                                    hideLoading(context);
                                    showError(context, e);}
                                  );
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(height: 13,),
                                    SvgPicture.asset('assets/clock.svg', color: colors.fFFFFF,),
                                    SizedBox(height: 10,),
                                    Text('Instant delivery',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: colors.fFFFFF
                                    ),)
                                  ],
                                )
                              ),
                      ),
                      SizedBox(
                        height: 75,
                        width: 159,
                        child: FilledButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStatePropertyAll<Color>(colors.fFFFFF,),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    side: BorderSide(width: 0, color: colors.fFFFFF),
                                    borderRadius: BorderRadius.circular(8),

                                  )
                              ),
                            ),
                            onPressed: (){},
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(height: 13,),
                                SvgPicture.asset('assets/calendar.svg', color: colors.a7A7A7,),
                                SizedBox(height: 10,),
                                Text('Scheduled delivery',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: colors.a7A7A7
                                  ),)
                              ],
                            )
                        ),
                      ),

                    ],
                  )

                ],
              ),
            ),
          )

        ],
      ),
    );
  }
}
