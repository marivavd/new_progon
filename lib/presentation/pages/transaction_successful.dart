import 'package:flutter/material.dart';
import 'package:new_progon/data/models/model_order.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/pages/home_page.dart';


class TransactionSuccessful extends StatefulWidget {
  TransactionSuccessful({super.key, required this.order});
  ModelOrder order;

  @override
  State<TransactionSuccessful> createState() => _TransactionSuccessfulState();
}

class _TransactionSuccessfulState extends State<TransactionSuccessful> with SingleTickerProviderStateMixin{
  late AnimationController animationController;
  bool finish = false;



  @override
  void initState(){
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 5000));
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    animationController.forward().whenComplete(() => setState((){finish = true;}));
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 114,),
                (!finish) ? RotationTransition(turns: Tween(begin: 1.0, end: 0.0).animate(animationController),
                    child: Image.asset('assets/Component 21.png')): Image.asset("assets/Good Tick.png"),
                SizedBox(height: 78,),
                Padding(padding: EdgeInsets.symmetric(horizontal: 24),child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    (finish)?Text('Transaction Successful',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: colors.x3A3A3A,
                        fontSize: 24
                      ),
                    ):SizedBox(height: 30,),
                    SizedBox(height: 8,),
                    Text(
                      "Your rider is on the way to your destination",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: colors.x3A3A3A,
                          fontSize: 14
                      ),
                    ),
                    SizedBox(height: 8,),
                    Row(
                      children: [
                        Text('Tracking Number ',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: colors.x3A3A3A,
                              fontSize: 14
                          ),
                        ),
                        Text('R-${widget.order.id}',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: colors.o560FA,
                              fontSize: 14
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 141,),
                    SizedBox(
                      height: 46,
                      width: 342,
                      child: OutlinedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll<Color>(colors.o560FA,),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  side: BorderSide(width: 0, color: colors.o560FA),
                                  borderRadius: BorderRadius.circular(4),

                                )
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(current_index: 2)));
                          },
                          child: Text("Track my item", style: TextStyle(
                              fontSize: 16,
                              color: colors.fFFFFF,
                              fontWeight: FontWeight.w700
                          ),)),
                    ),
                    SizedBox(height: 8,),
                    SizedBox(
                      height: 46,
                      width: 342,
                      child: OutlinedButton(
                          style: ButtonStyle(
                            side: MaterialStateProperty.all(
                                BorderSide(
                                  color: colors.o560FA,
                                  width: 1.0,
                                  style: BorderStyle.solid,)),
                            backgroundColor: MaterialStatePropertyAll<Color>(colors.fFFFFF,),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4),

                              ),
                            ),
                          ),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(current_index: 0)));
                          },
                          child: Text("Go back to homepage", style: TextStyle(
                              fontSize: 16,
                              color: colors.o560FA,
                              fontWeight: FontWeight.w700
                          ),)),
                    )

                  ],
                ),),
                SizedBox(height: 20,),

              ]),

        )
    );


  }




}
