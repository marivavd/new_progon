import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:new_progon/data/models/model_order.dart';
import 'package:new_progon/domain/create_destinations_text.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/pages/delivery.dart';

import '../widgets/text_field.dart';

class SendPackage2 extends StatefulWidget {

  SendPackage2({super.key, required this.order});
  ModelOrder order;


  @override
  State<SendPackage2> createState() => _SendPackage2State();
}

class _SendPackage2State extends State<SendPackage2> {
  String destinationText = '';


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      print(widget.order.id);
      destinationText = await createDestText(widget.order.id);
      setState(() {

      });
    });

  }
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return (destinationText == '')?Center(child: CircularProgressIndicator(),):Scaffold(
      body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  height: 108,
                  width: double.infinity,
                  alignment: Alignment.bottomLeft,
                  padding: const EdgeInsets.only(left: 14, right: 14, bottom: 19),
                  decoration: BoxDecoration(color: colors.fFFFFF, boxShadow: [
                    BoxShadow(
                        color: Color(0x26000026), blurRadius: 5, offset: Offset(0, 2))
                  ]),
                  child: Stack(children: [
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Text("Send a package",
                        style: TextStyle(
                            color: colors.a7A7A7,
                            fontWeight: FontWeight.w500,
                            fontSize: 16
                        ),),
                    ),
                    Align(alignment: Alignment.bottomLeft,
                        child: InkWell(
                          onTap: (){
                            Navigator.of(context).pop();
                          },
                          child: SvgPicture.asset('assets/arrow-square-right.svg', color: colors.o560FA),
                        ))
                  ]),
                ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 27,),
                    Text("Package Information",
                      style: TextStyle(
                        color: colors.o560FA,
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                      )
                    ),
                    SizedBox(height: 8,),
                    Text("Origin details",
                      style: TextStyle(
                          color: colors.x3A3A3A,
                          fontSize: 12,
                          fontWeight: FontWeight.w400
                      )
                    ),
                    SizedBox(height: 4,),
                    Row(
                      children: [
                        Text(
                            widget.order.address,
                            style: Theme.of(context).textTheme.labelSmall),
                        Text(
                            widget.order.country,
                            style: Theme.of(context).textTheme.labelSmall
                        )

                      ],
                    ),
                    SizedBox(height: 4,),
                    Text(
                        widget.order.phone,
                        style: Theme.of(context).textTheme.labelSmall
                    ),
                    SizedBox(height: 8,),
                    Text("Destination details", style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.x3A3A3A),),
                    Text(destinationText, style: Theme.of(context).textTheme.labelSmall),
                    SizedBox(height: 8,),
                    Text("Other details", style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.x3A3A3A),),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Package Items",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.order.items,
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Weight of items",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.order.weight.toString(),
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Worth of Items",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.order.worth.toString(),
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tracking Number",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "R-${widget.order.id}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 37,),
                    Divider(

                      color: Colors.black,
                    ),
                    SizedBox(height: 8,),
                    Text("Charges", style: TextStyle(
                        color: colors.o560FA,
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                    )),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Delivery Charges",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            widget.order.delivery_charges.toString(),
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Instant delivery",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.order.instantDelivery}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 7,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Tax and Service Charges",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.order.tax}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 9,),
                    Divider(
                      color: Colors.black,
                    ),
                    SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "Package total",
                            style: Theme.of(context).textTheme.labelSmall
                        ),
                        Text(
                            "${widget.order.sum_price}",
                            style: Theme.of(context).textTheme.labelSmall?.copyWith(color: colors.eC8000)
                        )
                      ],
                    ),
                    SizedBox(height: 46,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 158,
                            height: 48,
                            child: OutlinedButton(

                                style: ButtonStyle(
                                  side: MaterialStateProperty.all(
                                      BorderSide(
                                        color: Color(0xFF0560FA),
                                        width: 1.0,
                                        style: BorderStyle.solid,)),
                                  backgroundColor: MaterialStatePropertyAll<Color>(Colors.white,),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8),
                                          side: BorderSide(width: 1, color: Color(0xFF0560FA))
                                      )
                                  ),
                                ),
                                onPressed: (){
                                  Navigator.of(context).pop();
                                },
                                child: Text("Report", style: TextStyle(
                                    fontSize: 16,
                                    color: Color(0xFF0560FA),
                                    fontWeight: FontWeight.w700
                                ),))),
                        SizedBox(
                          width: 158,
                          height: 48,
                          child: OutlinedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA),),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    )
                                ),
                              ),
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionSuccessful(order: widget.order)));

                              },
                              child: Text("Successful", style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700
                              ),)),)
                      ],
                    )



                  ],
                ),

              )

            ],
          )),
    );
  }
}
