import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_progon/data/models/model_profile.dart';
import 'package:new_progon/domain/make_profile.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/pages/profile.dart';
import 'package:new_progon/presentation/pages/send_package_1.dart';


class HomePage extends StatefulWidget {
  HomePage({super.key, required this.current_index});
  int current_index;


  @override
  State<HomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  int index = 0;
  ModelProfile? profile;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    index = widget.current_index;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      profile = await makeProfile();
      setState(() {

      });
    });

  }



  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return (profile != null)?Scaffold(
      backgroundColor: colors.background,
      body: [Scaffold(backgroundColor: colors.background,
      body: Center(child: OutlinedButton(
        child: Text('Send a package'),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => SendPackage1())).then((value) => setState(() {}));
        },
      ),),), Scaffold(backgroundColor: colors.background,), Scaffold(backgroundColor: colors.background,) , Profile(profile: profile!)][index],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: colors.fFFFFF,
        type: BottomNavigationBarType.fixed,
        onTap: (val){
          setState(() {
            index = val;
          });
        },
        currentIndex: index,
        iconSize: 24,
        unselectedItemColor: colors.a7A7A7,

        selectedItemColor: colors.o560FA,
        unselectedLabelStyle: TextStyle(
            color: colors.a7A7A7,
            fontWeight: FontWeight.w400,
            fontSize: 12
        ),
        selectedLabelStyle: TextStyle(
            color: colors.o560FA,
            fontWeight: FontWeight.w400,
            fontSize: 12
        ),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        items: [
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/house.svg', color: (index == 0)?colors.o560FA:colors.a7A7A7,), label: "Home"),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/wallet.svg', color: (index == 1)?colors.o560FA:colors.a7A7A7,), label: "Wallet"),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/smart-car.svg', color: (index == 2)?colors.o560FA:colors.a7A7A7,), label: "Track"),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/profile-circle.svg', color: (index == 3)?colors.o560FA:colors.a7A7A7,), label: "Profile"),


        ],
      ),
    ):Center(child: CircularProgressIndicator(),);


  }




}

