import 'dart:async';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/domain/controllers/password_controller.dart';
import 'package:new_progon/domain/use_case_sign_up.dart';
import 'package:new_progon/domain/utils.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:new_progon/presentation/pages/sign_in.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/custom_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});


  @override
  State<SignUp> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignUp> {
  var fullname = TextEditingController();
  var phone = MaskedTextController(mask: '+0(000)000-00-00');
  var email = TextEditingController();
  var password = PasswordController();
  var passwordConfirm = PasswordController();
  bool check = false;
  SignUpUseCase useCase = SignUpUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty && passwordConfirm.text.isNotEmpty && fullname.text.isNotEmpty && phone.text.isNotEmpty && check;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 78,),
              Text(
                'Create an account',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Complete the sign up process to get started',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 33,),
              CustomField(label: 'Full name', hint: 'Ivanov Ivan', controller: fullname, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Phone Number', hint: '+7(999)999-99-99', controller: phone, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 24,),
              CustomField(label: 'Password', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Confirm Password', hint: '**********', controller: passwordConfirm, onChange: onChange, enableObscure: true,),
              SizedBox(height: 37,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 1,),
                  SizedBox(
                    height: 14,
                    width: 14,
                    child: Checkbox(
                      value: check,
                      side: BorderSide(
                          color: Color(0xFF006CEC),
                          width: 1
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2)
                      ),
                      activeColor: Color(0xFF006CEC),
                      onChanged: (val){
                        setState(() {
                          check = val!;
                          onChange;
                        });
                      },
                    ),
                  ),
                  SizedBox(width: 11,),
                  GestureDetector(
                    onTap: (){
                    },
                    child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: "By ticking this box, you agree to our ",
                            style: TextStyle(
                                color: Color(0xFFA7A7A7),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                            ),
                            children: [
                              TextSpan(
                                  text: 'Terms and\nconditions and private policy',
                                  style: TextStyle(
                                      color: Color(0xFFEBBC2E)
                                  )
                              )
                            ]
                        )),
                  )
                ],
              ),
              SizedBox(height: 64,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressButtonSignUp(fullname.text,
                          phone.text,
                          email.text,
                          password.hashPassword(),
                          passwordConfirm.hashPassword(),
                              (_){
                        hideLoading(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {}));
                        },
                          (String e)async{
                        hideLoading(context);
                        showError(context, e);
                          });
                    }: null,
                    child: Text(
                      "Sign Up",
                        style: Theme.of(context).textTheme.labelLarge
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Already have an account?",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));
                    },
                    child: Text(
                      "Sign in",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Column(
                  children: [
                    Text(
                      "or sign in using",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    SizedBox(height: 8,),
                    Image.asset("assets/Facebook google, apple.png")
                  ],
                )],
              ),
              SizedBox(height: 28,)


            ],

          ),
        ),
      ),
    );
  }
}
