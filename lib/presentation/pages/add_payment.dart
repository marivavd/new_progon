import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_progon/main.dart';

class AddPayment extends StatefulWidget {
  AddPayment({super.key});

  @override
  State<AddPayment> createState() => _AddPaymentState();
}

class _AddPaymentState extends State<AddPayment> {






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Column(
              children: [Container(
                height: 108,
                width: double.infinity,
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
                decoration: BoxDecoration(
                    color: colors.fFFFFF,
                    boxShadow: [BoxShadow(
                        color: Color(0x26000026),
                        blurRadius: 5,
                        offset: Offset(0, 2)
                    )]
                ),
                child: Stack(children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text("Add Payment method",
                        style: TextStyle(
                        color: colors.a7A7A7,
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                    ),
                    ),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();},
                        child: SvgPicture.asset('assets/arrow-square-right.svg', color: colors.o560FA),
                      )
                  )
                ]
                ),

              ),


              ]),

        )
    );


  }




}
