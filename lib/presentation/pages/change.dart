import 'dart:async';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/domain/controllers/password_controller.dart';
import 'package:new_progon/domain/use_case_change_pass.dart';
import 'package:new_progon/domain/use_case_sign_in.dart';
import 'package:new_progon/domain/use_case_sign_up.dart';
import 'package:new_progon/domain/utils.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:new_progon/presentation/pages/home_page.dart';
import 'package:new_progon/presentation/pages/sign_in.dart';
import 'package:new_progon/presentation/pages/sign_up.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/custom_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({super.key});


  @override
  State<ChangePass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ChangePass> {
  var password = PasswordController();
  var passwordConfirm = PasswordController();
  ChangePassUseCase useCase = ChangePassUseCase();

  bool isValid = false;


  void onChange(_){
    setState(() {
      isValid = password.text.isNotEmpty && passwordConfirm.text.isNotEmpty;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                'New Password',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Enter new password',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 87,),
              CustomField(label: 'Password', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Confirm Password', hint: '**********', controller: passwordConfirm, onChange: onChange, enableObscure: true,),
              SizedBox(height: 54,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressButtonChangePass(
                          password.hashPassword(),
                              passwordConfirm.hashPassword(),
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(current_index: 0,))).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          });
                    }: null,
                    child: Text(
                        "Log in",
                        style: Theme.of(context).textTheme.labelLarge
                    ),
                  ),
                ),
              ),

            ],

          ),
        ),
      ),
    );
  }
}
