import 'dart:async';

import 'package:flutter/material.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});


  @override
  State<MapPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MapPage> {
  List<ClusterizedPlacemarkCollection> cluster = [];
  Completer<YandexMapController> completer = Completer();





  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      final cluster_old = await createCluster();
      setState(() {
        cluster = [cluster_old];
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return (cluster.isEmpty)?Center(child: CircularProgressIndicator(),):
    Scaffold(
      backgroundColor: colors.background,
      body: YandexMap(
        onMapCreated: (YandexMapController controller){
          completer.complete(controller);
        },
        mapObjects: cluster,
      ),
    );
  }
}
