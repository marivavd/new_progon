import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_progon/data/models/model_profile.dart';
import 'package:new_progon/domain/make_profile.dart';
import 'package:new_progon/domain/use_case_log_out.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/pages/add_payment.dart';
import 'package:new_progon/presentation/pages/notification.dart';
import 'package:new_progon/presentation/pages/sign_in.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/list_tile.dart';


class Profile extends StatefulWidget {
  Profile({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<Profile> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Profile> {
  bool isVisible = true;
  bool light = false;
  LogOutUseCase useCase = LogOutUseCase();



  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 108,
              width: double.infinity,
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                  color: colors.fFFFFF,
                  boxShadow: [BoxShadow(
                      color: Color(0x26000026),
                      blurRadius: 5,
                      offset: Offset(0, 2)
                  )]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Profile',
                      style: TextStyle(
                        color: colors.a7A7A7,
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 27,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: Row(
                        children: [
                          (widget.profile.avatar == null)?
                          Image.asset('assets/avatar.png', height: 60, width: 60,):
                          ClipOval(
                              child: SizedBox(
                                child: Image.memory(widget.profile.avatar),
                                height: 60,
                                width: 60,
                              ) ),
                          SizedBox(width: 5,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Hello ${widget.profile.fullname}",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: colors.x3A3A3A
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Current balance: ",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: colors.x3A3A3A
                                    ),
                                  ),
                                  (isVisible)?Text("N${widget.profile.balance}:00",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: colors.o560FA
                                    ),):Text(
                                    "***************",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: colors.o560FA
                                    ),
                                  )

                                ],
                              )
                            ],
                          )

                        ],
                      )),
                      InkWell(
                        onTap: (){
                          setState(() {
                            isVisible = !isVisible;
                          });
                        },
                        child: SvgPicture.asset('assets/eye-slash_1.svg', color: colors.x3A3A3A,),
                      )
                    ],
                  ),
                  SizedBox(height: 19,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Enable dark Mode',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: colors.x3A3A3A
                        ),
                      ),
                      Switch(value: light,
                          activeColor: colors.o560FA,
                          activeTrackColor: Colors.white,

                          onChanged: (val){
                        setState(() {
                          MyApp.of(context).changeDiffTheme(context);
                          light = val;
                        });


                      }),

                    ],
                  ),
                  SizedBox(height: 19,),
                  ItemTile(title: "Edit Profile", icon: "assets/profile_1.svg", subtitle: "Name, phone no, address, email ...",),
                  SizedBox(height: 12,),
                  ItemTile(title: "Statements & Reports", icon: "assets/statement_1.svg", subtitle: "Download transaction details, orders, deliveries",),
                  SizedBox(height: 12,),
                  ItemTile(title: "Notification Settings", icon: "assets/notification_1.svg", subtitle: "mute, unmute, set location & tracking setting", ontap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationPage())).then((value) => setState(() {
                    }));
                  },),
                  SizedBox(height: 12,),
                  ItemTile(title: "Card & Bank account settings", icon: "assets/wallet-2.svg", subtitle: "change cards, delete card details", ontap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddPayment())).then((value) => setState(() {
                    }));
                  },),
                  SizedBox(height: 12,),
                  ItemTile(title: "Referrals", icon: "assets/carbon_two-person-lift.svg", subtitle: "check no of friends and earn",),
                  SizedBox(height: 12,),
                  ItemTile(title: "About Us", icon: "assets/map.svg", subtitle: "know more about us, terms and conditions",),
                  SizedBox(height: 12,),
                  ItemTile(title: "Log Out", icon: "assets/ic_round-log-out.svg", ontap: ()async{
                    showLoading(context);
                    useCase.pressButtonLogOut(
                            (_){
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {}));
                        },
                            (String e)async{
                          hideLoading(context);
                          showError(context, e);
                        }
                    );
                  },),
                  SizedBox(height: 12,),




                ],
              ),
            )
          ],
        ),
      )
    );


  }




}

