import 'dart:async';

import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:new_progon/domain/controllers/password_controller.dart';
import 'package:new_progon/domain/use_case_forgot.dart';
import 'package:new_progon/domain/use_case_sign_in.dart';
import 'package:new_progon/domain/use_case_sign_up.dart';
import 'package:new_progon/domain/utils.dart';
import 'package:new_progon/main.dart';
import 'package:new_progon/presentation/utils/create_cluster.dart';
import 'package:new_progon/presentation/pages/home_page.dart';
import 'package:new_progon/presentation/pages/otp.dart';
import 'package:new_progon/presentation/pages/sign_in.dart';
import 'package:new_progon/presentation/pages/sign_up.dart';
import 'package:new_progon/presentation/utils/show_dialogs.dart';
import 'package:new_progon/presentation/widgets/custom_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Forgot_pass extends StatefulWidget {
  const Forgot_pass({super.key});


  @override
  State<Forgot_pass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Forgot_pass> {
  var email = TextEditingController();
  ForgotUseCase useCase = ForgotUseCase();

  bool isRedEmail = false;
  bool isValid = false;


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail;
    });
  }






  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 155,),
              Text(
                'OTP',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Enter your email address',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 56,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 56,),

              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressButtonForgot(
                          email.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email.text))).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          });
                    }: null,
                    child: Text(
                        "Send OTP",
                        style: Theme.of(context).textTheme.labelLarge
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Remember password? Back to ",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));
                    },
                    child: Text(
                      "Sign in",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),
            ],

          ),
        ),
      ),
    );
  }
}
