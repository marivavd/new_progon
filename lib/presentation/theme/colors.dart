import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color o560FA;
  abstract final Color eC8000;
  abstract final Color x35B369;
  abstract final Color eBBC2E;
  abstract final Color x2F80ED;
  abstract final Color eD3A3A;
  abstract final Color x3A3A3A;
  abstract final Color x141414;
  abstract final Color fFFFFF;
  abstract final Color cFCFCF;
  abstract final Color a7A7A7;
  abstract final Color x292D32;
  abstract final background;
}

class LightColorsApp extends ColorsApp{
  @override
  Color get a7A7A7 => Color(0xFFA7A7A7);

  @override
  Color get cFCFCF => Color(0xFFCFCFCF);

  @override
  Color get eBBC2E => Color(0xFFEBBC2E);

  @override
  // TODO: implement eC8000
  Color get eC8000 => Color(0xFFEC8000);

  @override
  // TODO: implement eD3A3A
  Color get eD3A3A => Color(0xFFED3A3A);

  @override
  // TODO: implement fFFFFF
  Color get fFFFFF => Color(0xFFFFFFFF);

  @override
  // TODO: implement o560FA
  Color get o560FA => Color(0xFF0560FA);

  @override
  // TODO: implement x141414
  Color get x141414 => Color(0xFF141414);

  @override
  // TODO: implement x2F80ED
  Color get x2F80ED => Color(0xFF2F80ED);

  @override
  // TODO: implement x35B369
  Color get x35B369 => Color(0xFF35B369);

  @override
  // TODO: implement x3A3A3A
  Color get x3A3A3A => Color(0xFF3A3A3A);

  @override
  // TODO: implement x3A3A3A
  Color get background => Colors.white;

  @override
  // TODO: implement x292D32
  Color get x292D32 => Color(0xFF292D32);
}

class DarkColorsApp extends ColorsApp{
  @override
  Color get a7A7A7 => Color(0xFFFFFFFF);

  @override
  // TODO: implement x3A3A3A
  Color get background => Color(0xFF000D1D);

  @override
  Color get cFCFCF => Color(0xFFFFFFFF);

  @override
  Color get eBBC2E => Color(0xFFEBBC2E);

  @override
  // TODO: implement eC8000
  Color get eC8000 => Color(0xFFEC8000);

  @override
  // TODO: implement eD3A3A
  Color get eD3A3A => Color(0xFFED3A3A);

  @override
  // TODO: implement fFFFFF
  Color get fFFFFF => Color(0xFF000D1D);

  @override
  // TODO: implement o560FA
  Color get o560FA => Color(0xFF0560FA);

  @override
  // TODO: implement x141414
  Color get x141414 => Color(0xFF141414);

  @override
  // TODO: implement x2F80ED
  Color get x2F80ED => Color(0xFF2F80ED);

  @override
  // TODO: implement x35B369
  Color get x35B369 => Color(0xFF35B369);

  @override
  // TODO: implement x3A3A3A
  Color get x3A3A3A => Color(0xFFFFFFFF);

  @override
  // TODO: implement x292D32
  Color get x292D32 => Colors.white;

}