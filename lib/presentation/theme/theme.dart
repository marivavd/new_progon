import 'package:flutter/material.dart';
import 'package:new_progon/presentation/theme/colors.dart';

final colorsLights = LightColorsApp();
var light = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: colorsLights.x3A3A3A,
      fontWeight: FontWeight.w500,
      fontSize: 24,
      // height: 30
    ),
    labelSmall: TextStyle(
        color: colorsLights.a7A7A7,
        fontWeight: FontWeight.w400,
        fontSize: 12
    ),

    titleMedium: TextStyle(
        color: colorsLights.a7A7A7,
        fontWeight: FontWeight.w500,
        fontSize: 14,
        // height: 16
    ),
    labelLarge: TextStyle(
        color: colorsLights.fFFFFF,
        fontWeight: FontWeight.w700,
        fontSize: 16,
        //height: 16,
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.a7A7A7)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.a7A7A7)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.eD3A3A)
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          backgroundColor: colorsLights.o560FA,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsLights.a7A7A7,
          disabledForegroundColor: colorsLights.fFFFFF
      )
  ),


);
final colorsDark = DarkColorsApp();
var dark = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
        color: colorsDark.x3A3A3A,
        fontWeight: FontWeight.w500,
        fontSize: 24,
        //height: 30
    ),
    labelSmall: TextStyle(
        color: colorsDark.a7A7A7,
        fontWeight: FontWeight.w400,
        fontSize: 12
    ),

    titleMedium: TextStyle(
        color: colorsDark.a7A7A7,
        fontWeight: FontWeight.w500,
        fontSize: 14,
        //height: 16
    ),
    labelLarge: TextStyle(
      color: colorsDark.fFFFFF,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      //height: 16,
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsDark.a7A7A7)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsDark.a7A7A7)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsDark.eD3A3A)
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          backgroundColor: colorsDark.o560FA,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsDark.a7A7A7,
          disabledForegroundColor: colorsDark.fFFFFF
      )
  ),


);