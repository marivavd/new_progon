import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_progon/main.dart';

class ItemTile extends StatelessWidget{
  final String title;
  final String? subtitle;
  final String icon;
  final Function()? ontap;
  const ItemTile({super.key, required this.title, this.subtitle, required this.icon, this.ontap});

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Container(
      decoration: BoxDecoration(
          color: colors.fFFFFF,
          boxShadow: [
            BoxShadow(color: Color(0x26000026), offset: Offset(0, 2), blurRadius: 5)
          ]
      ),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 12),
        leading: SvgPicture.asset(icon, color: (title == 'Log Out')?colors.eD3A3A:colors.x3A3A3A),
        title: Text(title,
            style: TextStyle(
              color: colors.x3A3A3A,
              fontSize: 16,
              fontWeight: FontWeight.w500
            )
        ),
        subtitle: (subtitle != null) ? Text(subtitle!,
          style: TextStyle(
              color: colors.a7A7A7,
              fontSize: 12,
              fontWeight: FontWeight.w400
          )): null,
        trailing: SvgPicture.asset('assets/vector.svg', color: colors.x292D32,),
        onTap: (ontap != null) ? ontap!: null,
      ),
    );

  }

}
