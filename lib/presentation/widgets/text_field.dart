import 'package:flutter/material.dart';
import 'package:new_progon/main.dart';

class Custom_Field extends StatelessWidget{
  final String hint;
  final TextEditingController controller;
  const Custom_Field({required this.hint, required this.controller});



  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Container(
        decoration: BoxDecoration(
            color: colors.fFFFFF,
            boxShadow: [
              BoxShadow(
                  color: Color(0x26000026),
                  blurRadius: 5,
                  offset: Offset(0, 2)
              )
            ]
        ),
        child: SizedBox(
            height: 32,
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                hintText: hint,
                hintStyle: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: colors.cFCFCF,
                    fontFamily: 'Roboto'
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(0),
                  borderSide: BorderSide(width: 0, color: colors.fFFFFF)
                ),
              ),
            ))
    );
  }
}
