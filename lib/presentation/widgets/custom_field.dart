

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_progon/main.dart';

class CustomField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false, this.isValid = true, this.onChange});


  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;


  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.a7A7A7),
        ),
        SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            controller: widget.controller,
            obscuringCharacter: '*',
            obscureText: (widget.enableObscure) ? isObscure:false,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(color: colors.x3A3A3A),
            onChanged: widget.onChange,
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium!.copyWith(color: colors.cFCFCF),
              enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              suffixIconConstraints: const BoxConstraints(minWidth: 34),
              suffixIcon: (widget.enableObscure)?
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        isObscure = !isObscure;
                      });
                    },
                    child: SvgPicture.asset(
                        (!isObscure)
                            ? "assets/eye.svg"
                            : 'assets/eye-slash.svg',
                        color: colors.x141414,
                        //colorFilter: ColorFilter.mode(colors.x141414, BlendMode.color)
                    ),
                  ):null
            ),
          ),
        )

      ],
    );
  }


}
